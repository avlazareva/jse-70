package ru.t1.lazareva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.event.ConsoleEvent;

import static ru.t1.lazareva.tm.util.FormatUtil.formatBytes;

@Component
public final class SystemInfoListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "info";

    @NotNull
    private static final String DESCRIPTION = "Show system info.";

    @NotNull
    private static final String ARGUMENT = "-i";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@systemInfoListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SYSTEM INFO]");

        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("PROCESSOR: " + processorCount);
        System.out.println("MAX MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

}