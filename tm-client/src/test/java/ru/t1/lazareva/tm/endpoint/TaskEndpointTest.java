package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.lazareva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IUserEndpoint;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.dto.request.*;
import ru.t1.lazareva.tm.dto.response.*;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.AbstractException;
import ru.t1.lazareva.tm.marker.SoapCategory;
import ru.t1.lazareva.tm.service.PropertyService;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @Nullable
    public final static String ADMIN_LOGIN = "admin";
    @Nullable
    public final static String ADMIN_PASSWORD = "admin";
    @Nullable
    public final static String USER_LOGIN = "test1";
    @Nullable
    public final static String USER_PASSWORD = "test1";
    @Nullable
    public final static String USER_EMAIL = "USER_EMAIL";
    @Nullable
    public final static String TASK_NAME = "TASK_NAME";
    @Nullable
    public final static String TASK_DESC = "TASK_DESC";
    @Nullable
    public final static String TASK_NAME_NEW = "TASK_NAME_NEW";
    @Nullable
    public final static String TASK_DESC_NEW = "TASK_DESC_NEW";
    @Nullable
    public final static Status TASK_STATUS = Status.IN_PROGRESS;
    @Nullable
    public final static Sort TASK_SORT = Sort.BY_CREATED;
    @Nullable
    public final static String PROJECT_NAME = "PROJECT_NAME";
    @Nullable
    public final static String PROJECT_DESC = "PROJECT_DESC";
    @NotNull
    private final static IPropertyService PROPERTY_SERVICE = new PropertyService();
    @NotNull
    private final static IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);
    @NotNull
    private final static IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);
    @NotNull
    private final static ITaskEndpoint ENDPOINT = ITaskEndpoint.newInstance(PROPERTY_SERVICE);
    @NotNull
    private final static IProjectEndpoint PROJECT_ENDPOINT = IProjectEndpoint.newInstance(PROPERTY_SERVICE);
    @Nullable
    private static String ADMIN_TOKEN;
    @Nullable
    private static String USER_TOKEN;
    @Nullable
    private static TaskDto USER_TASK;
    @Nullable
    private static String USER_TASK_ID;
    @Nullable
    private static Integer USER_TASK_INTEGER = 0;
    @Nullable
    private static ProjectDto USER_PROJECT;
    @Nullable
    private static String USER_PROJECT_ID;

    @BeforeClass
    public static void before() throws AbstractException {
        @NotNull final UserLoginRequest loginAdminRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse loginAdminResponse = AUTH_ENDPOINT.login(loginAdminRequest);
        ADMIN_TOKEN = loginAdminResponse.getToken();

        @NotNull final UserRegistryRequest userRegistryRequest = new UserRegistryRequest(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        USER_ENDPOINT.registryUser(userRegistryRequest);

        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse loginUserResponse = AUTH_ENDPOINT.login(loginUserRequest);
        USER_TOKEN = loginUserResponse.getToken();

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(USER_TOKEN);
        projectCreateRequest.setName(PROJECT_NAME);
        projectCreateRequest.setDescription(PROJECT_DESC);
        @NotNull final ProjectCreateResponse projectCreateResponse = PROJECT_ENDPOINT.createProject(projectCreateRequest);
        USER_PROJECT = projectCreateResponse.getProject();
        USER_PROJECT_ID = USER_PROJECT.getId();

        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(USER_TOKEN);
        taskCreateRequest.setName(TASK_NAME);
        taskCreateRequest.setDescription(TASK_NAME);
        @NotNull final TaskCreateResponse taskCreateResponse = ENDPOINT.createTask(taskCreateRequest);
        USER_TASK = taskCreateResponse.getTask();
        USER_TASK_ID = USER_TASK.getId();
    }

    @AfterClass
    public static void after() throws AbstractException {
        ENDPOINT.clearTask(new TaskClearRequest(USER_TOKEN));
        USER_TASK = null;
        USER_TASK_ID = null;

        PROJECT_ENDPOINT.clearProject(new ProjectClearRequest(USER_TOKEN));
        USER_PROJECT = null;
        USER_PROJECT_ID = null;

        @NotNull final UserLogoutRequest logoutUserRequest = new UserLogoutRequest(USER_TOKEN);
        AUTH_ENDPOINT.logout(logoutUserRequest);
        USER_TOKEN = null;

        @NotNull final UserRemoveRequest removeUserRequest = new UserRemoveRequest(ADMIN_TOKEN);
        removeUserRequest.setLogin(USER_LOGIN);
        USER_ENDPOINT.removeUser(removeUserRequest);

        @NotNull final UserLogoutRequest logoutAdminRequest = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(logoutAdminRequest);
        ADMIN_TOKEN = null;
    }

    @Test
    public void getTaskById() throws AbstractException {
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(USER_TOKEN);
        request.setId(USER_TASK_ID);
        @Nullable final TaskShowByIdResponse response = ENDPOINT.showTaskById(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
    }

    @Test
    public void getTaskByIndex() throws AbstractException {
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(USER_TOKEN);
        request.setIndex(USER_TASK_INTEGER);
        @Nullable final TaskShowByIndexResponse response = ENDPOINT.showTaskByIndex(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
    }

    @Test
    public void getAllTasks() throws AbstractException {
        @NotNull final TaskListResponse responseNoSort = ENDPOINT.listTask(new TaskListRequest(USER_TOKEN));
        Assert.assertNotNull(responseNoSort);
        Assert.assertNotNull(responseNoSort.getTasks());

        @NotNull final TaskListRequest request = new TaskListRequest(USER_TOKEN);
        request.setSortType(TASK_SORT);
        @NotNull final TaskListResponse responseWithSort = ENDPOINT.listTask(request);
        Assert.assertNotNull(responseWithSort);
        Assert.assertNotNull(responseWithSort.getTasks());
    }


    @Test
    public void clearTasks() throws AbstractException {
        @NotNull final TaskClearResponse response = ENDPOINT.clearTask(new TaskClearRequest(USER_TOKEN));
        Assert.assertNotNull(response);

        @NotNull final TaskListRequest request = new TaskListRequest(USER_TOKEN);
        request.setSortType(TASK_SORT);
        @Nullable final TaskListResponse responseGetAll = ENDPOINT.listTask(request);
        Assert.assertNull(responseGetAll.getTasks());

        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(USER_TOKEN);
        taskCreateRequest.setName(TASK_NAME);
        taskCreateRequest.setDescription(TASK_DESC);
        @Nullable final TaskCreateResponse taskCreateResponse = ENDPOINT.createTask(taskCreateRequest);
        USER_TASK = taskCreateResponse.getTask();
        USER_TASK_ID = USER_TASK.getId();
    }

    @Test
    public void removeTaskById() throws AbstractException {
        @NotNull final TaskRemoveByIdRequest taskRemoveByIdRequest = new TaskRemoveByIdRequest(USER_TOKEN);
        taskRemoveByIdRequest.setId(USER_TASK_ID);
        @Nullable final TaskRemoveByIdResponse response = ENDPOINT.removeTaskById(taskRemoveByIdRequest);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());

        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(USER_TOKEN);
        taskCreateRequest.setName(TASK_NAME);
        taskCreateRequest.setDescription(TASK_DESC);
        @NotNull final TaskCreateResponse taskCreateResponse = ENDPOINT.createTask(taskCreateRequest);
        USER_TASK = taskCreateResponse.getTask();
        USER_TASK_ID = USER_TASK.getId();
    }


    @Test
    public void createTask() throws AbstractException {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(USER_TOKEN);
        request.setName(TASK_NAME);
        request.setDescription(TASK_DESC);
        @Nullable final TaskCreateResponse response = ENDPOINT.createTask(request);
        Assert.assertNotNull(response);
        @Nullable final TaskDto task = response.getTask();
        Assert.assertNotNull(task);
        @Nullable final TaskListByProjectIdRequest taskListByIdRequest = new TaskListByProjectIdRequest(USER_TOKEN);
        taskListByIdRequest.setTaskId(task.getId());
        Assert.assertNotNull(ENDPOINT.showTaskByProjectId(taskListByIdRequest));
    }

    @Test
    public void changeTaskStatusById() throws AbstractException {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(USER_TOKEN);
        request.setId(USER_TASK_ID);
        request.setStatus(TASK_STATUS);
        @Nullable final TaskChangeStatusByIdResponse response = ENDPOINT.changeTaskStatusById(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getStatus());
        Assert.assertEquals(TASK_STATUS, response.getTask().getStatus());
    }

    @Test
    public void changeTaskStatusByIndex() throws AbstractException {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(USER_TOKEN);
        request.setIndex(USER_TASK_INTEGER);
        request.setStatus(TASK_STATUS);
        @Nullable final TaskChangeStatusByIndexResponse response = ENDPOINT.changeTaskStatusByIndex(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getStatus());
        Assert.assertEquals(TASK_STATUS, response.getTask().getStatus());
    }

    @Test
    public void updateTaskById() throws AbstractException {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(USER_TOKEN);
        request.setId(USER_TASK_ID);
        request.setName(TASK_NAME_NEW);
        request.setDescription(TASK_DESC_NEW);
        @Nullable final TaskUpdateByIdResponse response = ENDPOINT.updateTaskById(request);
        Assert.assertNotNull(response);
        Assert.assertEquals(TASK_NAME_NEW, response.getTask().getName());
        Assert.assertEquals(TASK_DESC_NEW, response.getTask().getDescription());
    }

    @Test
    public void updateTaskByIndex() throws AbstractException {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(USER_TOKEN);
        request.setIndex(USER_TASK_INTEGER);
        request.setName(TASK_NAME_NEW);
        request.setDescription(TASK_DESC_NEW);
        @Nullable final TaskUpdateByIndexResponse response = ENDPOINT.updateTaskByIndex(request);
        Assert.assertNotNull(response);
        Assert.assertEquals(TASK_NAME_NEW, response.getTask().getName());
        Assert.assertEquals(TASK_DESC_NEW, response.getTask().getDescription());
    }

    @Test
    public void startTaskById() throws AbstractException {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(USER_TOKEN);
        request.setId(USER_TASK_ID);
        @Nullable final TaskStartByIdResponse response = ENDPOINT.startTaskById(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void startTaskByIndex() throws AbstractException {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(USER_TOKEN);
        request.setIndex(USER_TASK_INTEGER);
        @Nullable final TaskStartByIndexResponse response = ENDPOINT.startTaskByIndex(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void completeTaskById() throws AbstractException {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(USER_TOKEN);
        request.setId(USER_TASK_ID);
        @Nullable final TaskCompleteByIdResponse response = ENDPOINT.completeTaskById(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getStatus());
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test
    public void completeTaskByIndex() throws AbstractException {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(USER_TOKEN);
        request.setIndex(USER_TASK_INTEGER);
        @Nullable final TaskCompleteByIndexResponse response = ENDPOINT.completeTaskByIndex(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getStatus());
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(USER_TOKEN);
        request.setTaskId(USER_TASK_ID);
        request.setProjectId(USER_PROJECT_ID);
        @NotNull final TaskBindToProjectResponse response = ENDPOINT.bindTaskToProject(request);
        Assert.assertNotNull(response);
        response.getTask();
        response.getTask().getProjectId();
        Assert.assertNotNull(response.getTask().getProjectId());
        Assert.assertEquals(USER_PROJECT.getId(), response.getTask().getProjectId());
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        @NotNull final TaskUnbindToProjectRequest request = new TaskUnbindToProjectRequest(USER_TOKEN);
        request.setTaskId(USER_TASK_ID);
        request.setProjectId(USER_PROJECT_ID);
        @NotNull final TaskUnbindToProjectResponse response = ENDPOINT.unbindTaskFromProject(request);
        Assert.assertNotNull(response);
        Assert.assertNull(response.getTask().getProjectId());
    }

    @Test
    public void getTasksByProjectId() throws AbstractException {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(USER_TOKEN);
        request.setTaskId(USER_TASK_ID);
        request.setProjectId(USER_PROJECT_ID);
        ENDPOINT.bindTaskToProject(request);
        @NotNull final TaskListByProjectIdRequest taskListByProjectIdRequest = new TaskListByProjectIdRequest(USER_TOKEN);
        taskListByProjectIdRequest.setProjectId(USER_PROJECT_ID);
        @NotNull final TaskListByProjectIdResponse response = ENDPOINT.showTaskByProjectId(taskListByProjectIdRequest);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTasks());
    }

}
