package ru.t1.lazareva.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.dto.TaskDto;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.CustomUser;

@Controller
public class TaskController {

    @Autowired
    private ITaskDtoService taskService;

    @Autowired
    private IProjectDtoService projectService;

    @GetMapping("/task/create")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public String create(@AuthenticationPrincipal final CustomUser user) {
        taskService.save(user.getUserId(), new TaskDto("New Task" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeOneById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") TaskDto task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.save(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ModelAndView edit(@AuthenticationPrincipal final CustomUser user, @PathVariable("id") String id) {
        final TaskDto task = taskService.findOneById(user.getUserId(), id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projectService.findAll(user.getUserId()));
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}