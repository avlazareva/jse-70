package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.dto.ProjectDto;
import ru.t1.lazareva.tm.model.CustomUser;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @Nullable
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ProjectDto get(@AuthenticationPrincipal final CustomUser user, @NotNull @PathVariable("id") String id) {
        return projectService.findOneById(user.getUserId(), id);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void post(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody ProjectDto project) {
        projectService.save(user.getUserId(), project);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void put(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody ProjectDto project) {
        projectService.save(user.getUserId(), project);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void delete(@AuthenticationPrincipal final CustomUser user, @NotNull @PathVariable("id") String id) {
        projectService.removeOneById(user.getUserId(), id);
    }

}