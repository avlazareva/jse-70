package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.dto.ProjectDto;

import java.util.Collection;
import java.util.List;

public interface IProjectDtoService {

    void save(final String userId, final ProjectDto project);

    void saveAll(final String userId, final Collection<ProjectDto> projects);

    void removeAll(final String userId);

    void removeOneById(final String userId, final String id);

    List<ProjectDto> findAll(final String userId);

    ProjectDto findOneById(final String userId, final String id);

}