package ru.t1.lazareva.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.lazareva.tm.dto.TaskDto;

import javax.xml.bind.annotation.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "tasksDeleteRequest")
public class TasksDeleteRequest {

    @XmlElement(required = true)
    protected List<TaskDto> tasks;

}