package ru.t1.lazareva.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.lazareva.tm.dto.TaskDto;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskSaveRequest")
public class TaskSaveRequest {

    @XmlElement(required = true)
    protected TaskDto task;

}
