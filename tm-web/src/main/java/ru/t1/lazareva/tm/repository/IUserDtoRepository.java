package ru.t1.lazareva.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.UserDto;

@Repository
public interface IUserDtoRepository extends JpaRepository<UserDto, String> {

    UserDto findByLogin(final String login);

}