package ru.t1.lazareva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.TaskDto;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends JpaRepository<TaskDto, String> {

    void deleteAllByUserId(@NotNull String userId);

    void deleteByIdAndUserId(@NotNull String id, @NotNull String userId);

    @NotNull
    List<TaskDto> findAllByUserId(@NotNull String userId);

    @Nullable
    TaskDto findByIdAndUserId(@NotNull String id, @NotNull String userId);

}