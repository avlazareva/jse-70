package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.dto.TaskDto;
import ru.t1.lazareva.tm.model.CustomUser;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Nullable
    @GetMapping()
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public List<TaskDto> get(@AuthenticationPrincipal final CustomUser user) {
        return taskService.findAll(user.getUserId());
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void post(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody List<TaskDto> tasks) {
        taskService.saveAll(user.getUserId(), tasks);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void put(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody List<TaskDto> tasks) {
        taskService.saveAll(user.getUserId(), tasks);
    }

    @DeleteMapping()
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void delete(@AuthenticationPrincipal final CustomUser user) {
        taskService.removeAll(user.getUserId());
    }

}