package ru.t1.lazareva.tm;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.client.ProjectRestEndpointClient;
import ru.t1.lazareva.tm.dto.ProjectDto;
import ru.t1.lazareva.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    final ProjectDto project1 = new ProjectDto("Test Project 1");

    @NotNull
    final ProjectDto project2 = new ProjectDto("Test Project 2");


    @Before
    public void init() {
        client.post(project1);
    }

    @After
    public void clear() {
        client.delete(project1.getId());
    }

    @Test
    public void testGet() {
        Assert.assertNotNull(client.get(project1.getId()));
        Assert.assertThrows(FeignException.class, () -> client.get(project2.getId()));
    }

    @Test
    public void testPost() {
        Assert.assertThrows(FeignException.class, () -> client.get(project2.getId()));
        client.post(project2);
        Assert.assertNotNull(client.get(project2.getId()));
        client.delete(project2.getId());
    }

    @Test
    public void testPut() {
        Assert.assertNull(client.get(project1.getId()).getDescription());
        project1.setDescription("Description 1");
        client.put(project1);
        Assert.assertEquals("Description 1", client.get(project1.getId()).getDescription());
        project1.setDescription(null);
        client.put(project1);
        Assert.assertNull(client.get(project1.getId()).getDescription());
    }

    @Test
    public void testDelete() {
        Assert.assertNotNull(client.get(project1.getId()));
        client.delete(project1.getId());
        Assert.assertThrows(FeignException.class, () -> client.get(project1.getId()));
        client.post(project1);
    }

}
