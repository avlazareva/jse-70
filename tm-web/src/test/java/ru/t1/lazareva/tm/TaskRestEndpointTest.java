package ru.t1.lazareva.tm;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.client.TaskRestEndpointClient;
import ru.t1.lazareva.tm.dto.TaskDto;
import ru.t1.lazareva.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    final TaskDto task2 = new TaskDto("Test Task 2");


    @Before
    public void init() {
        client.post(task1);
    }

    @After
    public void clear() {
        client.delete(task1.getId());
    }

    @Test
    public void testGet() {
        Assert.assertNotNull(client.get(task1.getId()));
        Assert.assertThrows(FeignException.class, () -> client.get(task2.getId()));
    }

    @Test
    public void testPost() {
        Assert.assertThrows(FeignException.class, () -> client.get(task2.getId()));
        client.post(task2);
        Assert.assertNotNull(client.get(task2.getId()));
        client.delete(task2.getId());
    }

    @Test
    public void testPut() {
        Assert.assertNull(client.get(task1.getId()).getDescription());
        task1.setDescription("Description");
        client.put(task1);
        Assert.assertEquals("Description", client.get(task1.getId()).getDescription());
        task1.setDescription(null);
        client.put(task1);
        Assert.assertNull(client.get(task1.getId()).getDescription());
    }

    @Test
    public void testDelete() {
        Assert.assertNotNull(client.get(task1.getId()));
        client.delete(task1.getId());
        Assert.assertThrows(FeignException.class, () -> client.get(task1.getId()));
        client.post(task1);
    }

}
