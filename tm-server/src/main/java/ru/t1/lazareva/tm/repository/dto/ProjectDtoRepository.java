package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.model.ProjectDto;

@Repository
@Scope("prototype")
public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> {

    @NotNull
    ProjectDto create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

    @NotNull
    ProjectDto create(
            @NotNull final String userId,
            @NotNull final String name
    );

}


