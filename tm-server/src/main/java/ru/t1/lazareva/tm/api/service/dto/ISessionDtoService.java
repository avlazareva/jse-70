package ru.t1.lazareva.tm.api.service.dto;

import ru.t1.lazareva.tm.dto.model.SessionDto;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDto> {
}
