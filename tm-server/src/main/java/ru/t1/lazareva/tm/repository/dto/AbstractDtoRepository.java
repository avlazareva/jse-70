package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.model.AbstractModelDto;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public interface AbstractDtoRepository<M extends AbstractModelDto> extends JpaRepository<M, String> {

    @NotNull
    String getSortType(@NotNull Comparator comparator);

    @NotNull
    M add(@NotNull final M model);

    @NotNull
    Collection<M> add(@NotNull final Collection<M> models);

    void clear();

    @Override
    boolean existsById(@NotNull final String id);

    boolean existsByIndex(@NotNull final Integer index);

    @Nullable
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull final Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull final String id);

    @Nullable
    M findOneByIndex(@NotNull final Integer index);

    int getSize();

    void remove(@NotNull final M model);

    M update(@NotNull final M model);

}
